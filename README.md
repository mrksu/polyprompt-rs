# polyprompt: randomize your shell prompt

polyprompt is a tiny program that suggests a random character to use instead of `$` or `%` in your shell prompt.

## Example

* Before:

    ```
    me@my-computer ~ $
    ```

* After:

    ```
    me@my-computer ~ 💬
    me@my-computer ~ ➤
    me@my-computer ~ 🤸
    ```

## Usage

1. Install this program:

    ```
    $ cargo install --git https://gitlab.com/msuchane/polyprompt
    ```

2. To generate a random prompt character, run the program:

    ```
    $ polyprompt

    🐧
    ```

3. To have a brand new character for each shell session, add polyprompt to your shell configuration file.

    For example, if you use bash, you might add the following at the end of your `~/.bashrc` file:

    ```
    random_character=$(polyprompt)
    export PS1=$(echo $PS1 | sed s/\\$/${random_character}/g)
    ```
